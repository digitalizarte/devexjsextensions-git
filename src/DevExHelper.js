﻿/**
 * devExCreateComboboxAC
 * Add ASPxCombobox control autocomplete functionality using ajax callbacks.
 * @autor Eiff Damian - damian@digitalizarte.com.ar
 * @history 2013-01-18
 */
devExCreateComboboxAC = function devExCreateComboboxAC() {
        var controlsContext = {},
        /**
         * getContext 
         * Return combobox context vars. 
         * @private
         * @return {object}
         */
        getContext = function getContext(s) {
                var retContext = null;
                if (s) {
                        var id = s.GetInputElement().id;
                        retContext = controlsContext[id];
                }
                return retContext;
        },
        /**
         * activateComboboxAC
         * Starts a timer when capture last significant keydown event.
         * @private
         * @return {void}    
         */
        activateComboboxAC = function activateComboboxAC(s) {
                if (s) {
                        var context = getContext(s);
                        clearTimeout(context.keyTimeoutId);
                        context.keyTimeoutId = setTimeout(function () {
                                activateNowComboboxAC.call(s, s);
                        }, context.timeout);
                }
        },
        activateNowComboboxAC = function activateNowComboboxAC(s) {
                if (s) {
                        var context = getContext(s);
                        if (!s.InCallback()) {
                                var value = s.GetTextInternal();
                                if (value && value.length >= 3 && value !== context.lastProcessedValue) {
                                        var itemsCnt = s.GetItemCount();
                                        if (!itemsCnt || (itemsCnt && !s.GetListBoxControl().IsDisplayed())) {
                                                s.ClearItems();
                                                context.lastProcessedValue = value;
                                                if (context.applyFilter) {
                                                        s.filterStrategy.ClearFilter();
                                                        s.filterStrategy.filter = value;
                                                }
                                                s.PerformCallback(value);
                                        }
                                }
                        } else {
                                if (context.inCallbackRetry < 10) {
                                        setTimeout(function () {
                                                activateComboboxAC.call(s, s);
                                        }, context.timeout);
                                }
                                context.inCallbackRetry++;
                        }
                }
        },
        onKeyDownComboboxAC = function onKeyDownComboboxAC(s, e) {
                var context = getContext(s);
                context.lastKeyPressed = e.htmlEvent.keyCode;
                switch (context.lastKeyPressed) {
                        // ignore navigational & special keys                                                                      
                        case 9: // tab
                        case 13: // return
                        case 16: // shift
                        case 17: // ctrl
                        case 18: // alt
                        case 27: // escape
                        case 35: // end
                        case 36: // home
                        case 37: // left
                        case 39: // right
                        case 38: // up
                        case 40: // down
                                break;

                        default:
                                activateComboboxAC.call(s, s);
                                break;
                }
        },
        onKeyUpComboboxAC = function onKeyUpComboboxAC(s, e) {
                var context = getContext(s);
                context.lastTypeValue = s.GetTextInternal();
        },
        onBeginCallbackComboboxAC = function onBeginCallbackComboboxAC(s, e) {
                var context = getContext(s);
                context.inCallback = true;
                context.inCallbackRetry = 0;
        },
        onEndCallbackComboboxAC = function onEndCallbackComboboxAC(s, e) {
                var context = getContext(s);
                context.inCallback = false;
                s.SetText(context.lastTypeValue);
                if (s.GetItemCount() > 0) {
                        if (context.applyFilter) {
                                s.filterStrategy.filter = context.lastProcessedValue;
                                s.filterStrategy.Filtering();
                        }
                        s.ShowDropDown();
                        // Hack to hide Headers on multi column combo.
                        if (context.hideHeader) {
                                var e = document.getElementById(s.name + '_DDD_L_H');
                                if (e && e.style) {
                                        e.style.display = 'none';
                                }
                        }
                }
        },
        onValueChangedComboboxAC = function onValueChangedComboboxAC(s, e) {
                var context = getContext(s);
                clearTimeout(context.keyTimeoutId);
                var text = s.GetTextInternal(),
                        idx = s.GetSelectedIndex();
                if (idx > -1) {
                        item = s.GetSelectedItem();
                        s.ClearItems();
                        s.SetTextInternal(text);
                        context.lastProcessedValue = text;
                        context.onSelected.call(s, s, item, idx);
                }
        },
        devExCreateComboboxACFn = function devExCreateComboboxACFn(cbo, config) {
                if (cbo) {
                        // Validate and prepare config.
                        config = config || {};
                        config.timeout = config.timeout || 750;
                        config.minChars = config.minChars || 3;
                        config.onSelected = config.onSelected || function () {};

                        var id = cbo.GetInputElement().id;
                        // Init combo state.
                        controlsContext[id] = {
                                keyTimeoutId: 0,
                                lastKeyPressed: 0,
                                lastProcessedValue: false,
                                lastTypeValue: false,
                                inCallback: false,
                                inCallbackRetry: 0,
                                timeout: config.timeout,
                                onSelected: config.onSelected,
                                minChars: config.minChars,
                                hideHeader: config.hideHeader || false,
                                applyFilter: cbo.incrementalFilteringMode && cbo.incrementalFilteringMode !== 'None'
                        };

                        // Bind Events with handlers.
                        cbo.KeyDown.AddHandler(onKeyDownComboboxAC);

                        cbo.KeyUp.AddHandler(onKeyUpComboboxAC);

                        cbo.BeginCallback.AddHandler(onBeginCallbackComboboxAC);

                        cbo.EndCallback.AddHandler(onEndCallbackComboboxAC);

                        cbo.ValueChanged.AddHandler(onValueChangedComboboxAC);
                }
        };
        // Returns public interface.
        return devExCreateComboboxACFn;
}();
/**
 * devExAddContextMenuHandlerGridView
 * Add ContextMenu Handler to GridView.
 * @autor Eiff Damian - damian@digitalizarte.com.ar
 * @history 2013-01-30
 * @example:
 *           devExAddContextMenuHandlerGridView(dxgGrid, function(s,e){ console.log('ContextMenu event.'); });
 */
devExAddContextMenuHandlerGridView = function devExAddContextMenuHandlerGridView() {
        var controlsContext = {},
        /**
         * getContext 
         * Return combobox context vars. 
         * @private
         * @return {object}
         */
        getContext = function getContext(s) {
                var retContext = null;
                if (s) {
                        var name = s.name;
                        retContext = controlsContext[name];
                }
                return retContext;
        },
        devExAddCMHandlerGVFn = function devExAddContextMenuHandlerGridViewFn(grid, handler) {
                if (grid) {
                        var context = getContext(grid),
                                tbl = grid.GetMainTable(),
                                rows = tbl.rows;
                        for (var i = 1, l = rows.length; i < l; i++) {
                                var row = rows[i],
                                        cls = row.getAttribute('class');
                                if (cls && (cls.indexOf('dxgvDataRow_') > -1 || cls.indexOf('dxgvFocusedRow_') > -1)) {
                                        var onEvent = row.getAttribute('oncontextmenu');
                                        if (!onEvent) {
                                                var evtCode = "aspxGVContextMenu('" + grid.name + "','row'," + String(i - 1) + ",event); return false;";
                                                row.setAttribute('oncontextmenu', evtCode);
                                        }
                                }
                        }

                        // Check if not assign.
                        if (!context) {
                                // Add ContextMenu handler.
                                grid.ContextMenu.AddHandler(handler);
                                if (grid.callBacksEnabled) {

                                        grid.BeginCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                context.lastCommand = e.command;
                                        });

                                        var setupFn = arguments.callee;
                                        grid.EndCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                if (context.lastCommand != 'FUNCTION') {
                                                        setupFn.call(this, s, handler);
                                                }
                                        });
                                }
                                controlsContext[grid.name] = {
                                        lastCommand: ''
                                };
                        }
                }
        };
        // Returns public interface.
        return devExAddCMHandlerGVFn;
}();


/**
 * devExAddRowClickHandlerGridView
 * Add RowClick Handler to GridView.
 * @autor Eiff Damian - damian@digitalizarte.com.ar
 * @history 2013-02-06
 * @example:
 *           devExAddRowClickHandlerGridView(dxgGrid, function(s,e){ console.log('RowClick event.'); });
 */
devExAddRowClickHandlerGridView = function devExAddRowClickHandlerGridView() {
        var controlsContext = {},
        /**
         * getContext 
         * Return combobox context vars. 
         * @private
         * @return {object}
         */
        getContext = function getContext(s) {
                var retContext = null;
                if (s) {
                        var name = s.name;
                        retContext = controlsContext[name];
                }
                return retContext;
        },
        devExAddCMHandlerGVFn = function devExAddRowClickHandlerGridViewFn(grid, handler) {
                if (grid) {
                        var context = getContext(grid),
                                tbl = grid.GetMainTable(),
                                onEvent = tbl.getAttribute('onclick');
                        if (!onEvent) {
                                var evtCode = "aspxGVTableClick('" + grid.name + "',event);";
                                tbl.setAttribute('onclick', evtCode);

                        }

                        // Check if not assign.
                        if (!context) {
                                // Add RowClick handler.
                                grid.RowClick.AddHandler(handler);
                                if (grid.callBacksEnabled) {

                                        grid.BeginCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                context.lastCommand = e.command;
                                        });

                                        var setupFn = arguments.callee;
                                        grid.EndCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                if (context.lastCommand != 'FUNCTION') {
                                                        setupFn.call(this, s, handler);
                                                }
                                        });
                                }
                                controlsContext[grid.name] = {
                                        lastCommand: ''
                                };
                        }
                }
        };
        // Returns public interface.
        return devExAddCMHandlerGVFn;
}();

/**
 * devExAddRowDblClickHandlerGridView
 * Add RowDblClick Handler to GridView.
 * @autor Eiff Damian - damian@digitalizarte.com.ar
 * @history 2013-02-06
 * @example:
 *           devExAddRowDblClickHandlerGridView(dxgGrid, function(s,e){ console.log('RowDblClick event.'); });
 */
devExAddRowDblClickHandlerGridView = function devExAddRowDblClickHandlerGridView() {
        var controlsContext = {},
        /**
         * getContext 
         * Return combobox context vars. 
         * @private
         * @return {object}
         */
        getContext = function getContext(s) {
                var retContext = null;
                if (s) {
                        var name = s.name;
                        retContext = controlsContext[name];
                }
                return retContext;
        },
        devExAddCMHandlerGVFn = function devExAddRowDblClickHandlerGridViewFn(grid, handler) {
                if (grid) {
                        var context = getContext(grid),
                                tbl = grid.GetMainTable(),
                                onEvent = tbl.getAttribute('ondblclick');
                        if (!onEvent) {
                                var evtCode = "aspxGVTableDblClick('" + grid.name + "',event);";
                                tbl.setAttribute('ondblclick', evtCode);
                        }

                        // Check if not assign.
                        if (!context) {
                                // Add RowDblClick handler.
                                grid.RowDblClick.AddHandler(handler);
                                if (grid.callBacksEnabled) {

                                        grid.BeginCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                context.lastCommand = e.command;
                                        });

                                        var setupFn = arguments.callee;
                                        grid.EndCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                if (context.lastCommand != 'FUNCTION') {
                                                        setupFn.call(this, s, handler);
                                                }
                                        });
                                }
                                controlsContext[grid.name] = {
                                        lastCommand: ''
                                };
                        }
                }
        };
        // Returns public interface.
        return devExAddCMHandlerGVFn;
}();

/**
 * devExAddTextChangedHandlerTextbox
 * Add TextChanged Handler to Edit.
 * @autor Eiff Damian - damian@digitalizarte.com.ar
 * @history 2013-02-06
 * @example:
 *           devExAddTextChangedHandlerTextbox(dxgGrid, function(s,e){ console.log('TextChanged event.'); });
 */
devExAddTextChangedHandlerTextbox = function devExAddTextChangedHandlerTextbox() {
        var controlsContext = {},
        /**
         * getContext 
         * Return combobox context vars. 
         * @private
         * @return {object}
         */
        getContext = function getContext(s) {
                var retContext = null;
                if (s) {
                        var name = s.name;
                        retContext = controlsContext[name];
                }
                return retContext;
        },
        devExAddTextChangedHandlerTextboxFn = function devExAddTextChangedHandlerTextboxFn(edit, handler) {
                if (edit) {
                        var context = getContext(edit),
                                input = edit.GetInputElement(),
                                onEvent = input.getAttribute('onchange');
                        if (!onEvent) {
                                var evtCode = "aspxEValueChanged('" + edit.name + "')";
                                input.setAttribute('onchange', evtCode);
                        }

                        // Check if not assign.
                        if (!context) {
                                // Add TextChanged handler.
                                edit.TextChanged.AddHandler(handler);
                                if (edit.callBacksEnabled) {

                                        edit.BeginCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                context.lastCommand = e.command;
                                        });

                                        var setupFn = arguments.callee;
                                        edit.EndCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                if (context.lastCommand != 'FUNCTION') {
                                                        setupFn.call(this, s, handler);
                                                }
                                        });
                                }
                                controlsContext[edit.name] = {
                                        lastCommand: ''
                                };
                        }
                }
        };
        // Returns public interface.
        return devExAddTextChangedHandlerTextboxFn;
}();


/**
 * devExAddKeyPressHandlerTextbox
 * Add KeyPress Handler to Edit.
 * @autor Eiff Damian - damian@digitalizarte.com.ar
 * @history 2013-02-06
 * @example:
 *           devExAddKeyPressHandlerTextbox(dxgGrid, function(s,e){ console.log('KeyPress event.'); });
 */
devExAddKeyPressHandlerTextbox = function devExAddKeyPressHandlerTextbox() {
        var controlsContext = {},
        /**
         * getContext 
         * Return combobox context vars. 
         * @private
         * @return {object}
         */
        getContext = function getContext(s) {
                var retContext = null;
                if (s) {
                        var name = s.name;
                        retContext = controlsContext[name];
                }
                return retContext;
        },
        devExAddKeyPressHandlerTextboxFn = function devExAddKeyPressHandlerTextboxFn(edit, handler) {
                if (edit) {
                        var context = getContext(edit),
                                input = edit.GetInputElement(),
                                onEvent = input.getAttribute('onkeypress');
                        if (!onEvent) {
                                var evtCode = "aspxEKeyPress('" + edit.name + "',event)";
                                input.setAttribute('onkeypress', evtCode);
                        }

                        // Check if not assign.
                        if (!context) {
                                // Add KeyPress handler.
                                edit.KeyPress.AddHandler(handler);
                                if (edit.callBacksEnabled) {

                                        edit.BeginCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                context.lastCommand = e.command;
                                        });

                                        var setupFn = arguments.callee;
                                        edit.EndCallback.AddHandler(function (s, e) {
                                                var context = getContext(s);
                                                if (context.lastCommand != 'FUNCTION') {
                                                        setupFn.call(this, s, handler);
                                                }
                                        });
                                }
                                controlsContext[edit.name] = {
                                        lastCommand: ''
                                };
                        }
                }
        };
        // Returns public interface.
        return devExAddKeyPressHandlerTextboxFn;
}();
