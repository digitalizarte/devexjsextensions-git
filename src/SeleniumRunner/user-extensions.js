/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */
/**
 * dateFormat
 * @url: http://blog.stevenlevithan.com/archives/date-time-format
 */
dateFormat = (function () {
    // DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
    var token = /d{1,4}|M{1,4}|yy(?:yy)?|([HhmsTt])\1?|[LloSZ]|'[^']*'|'[^']*'/g,
        timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
        timezoneClip = /[^-+\dA-Z]/g,
        pad = function (val, len) {
            val = String(val);
            len = len || 2;
            while (val.length < len) {
                val = '0' + val;
            }
            return val;
        };

    // Regexes and supporting functions are cached through closure
    return function (date, mask, utc) {
        // DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        var dF = dateFormat;

        // You can't provide utc if you skip other args (use the 'UTC:' mask prefix)
        if (arguments.length == 1 && Object.prototype.toString.call(date) == '[object String]' && !/\d/.test(date)) {
            mask = date;
            date = undefined;
        }

        // Passing date through Date applies Date.parse, if necessary
        date = date ? new Date(date) : new Date;
        if (isNaN(date)) {
            throw SyntaxError('invalid date');
        }

        // DEBUG(dateFormat.masks);
        mask = String(dF.masks[mask] || mask || dF.masks['default']);

        // Allow setting the utc argument via the mask
        if (mask.slice(0, 4) == 'UTC:') {
            mask = mask.slice(4);
            utc = true;
        }

        var _ = utc ? 'getUTC' : 'get',
            d = date[_ + 'Date'](),
            D = date[_ + 'Day'](),
            M = date[_ + 'Month'](),
            y = date[_ + 'FullYear'](),
            H = date[_ + 'Hours'](),
            m = date[_ + 'Minutes'](),
            s = date[_ + 'Seconds'](),
            L = date[_ + 'Milliseconds'](),
            o = utc ? 0 : date.getTimezoneOffset(),
            flags = {
                d: d,
                dd: pad(d),
                ddd: dF.i18n.dayNames[D],
                dddd: dF.i18n.dayNames[D + 7],
                M: M + 1,
                MM: pad(M + 1),
                MMM: dF.i18n.monthNames[M],
                MMMM: dF.i18n.monthNames[M + 12],
                yy: String(y).slice(2),
                yyyy: y,
                h: H % 12 || 12,
                hh: pad(H % 12 || 12),
                H: H,
                HH: pad(H),
                m: m,
                mm: pad(m),
                s: s,
                ss: pad(s),
                l: pad(L, 3),
                L: pad(L > 99 ? Math.round(L / 10) : L),
                t: H < 12 ? 'a' : 'p',
                tt: H < 12 ? 'am' : 'pm',
                T: H < 12 ? 'A' : 'P',
                TT: H < 12 ? 'AM' : 'PM',
                Z: utc ? 'UTC' : (String(date).match(timezone) || ['']).pop().replace(timezoneClip, ''),
                o: (o > 0 ? '-' : '+') + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
                S: ['th', 'st', 'nd', 'rd'][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
            };


        return mask.replace(token, function ($0) {
            return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
        });
    };
})();

// Some common format strings
dateFormat.masks = {
    "default": "ddd MMM dd yyyy HH:MM:ss",
    shortDate: "M/d/yy",
    mediumDate: "MMM d, yyyy",
    longDate: "MMMM d, yyyy",
    fullDate: "dddd, MMMM d, yyyy",
    shortTime: "h:mm TT",
    mediumTime: "h:mm:ss TT",
    longTime: "h:mm:ss TT Z",
    isoDate: "yyyy-MM-dd",
    isoTime: "HH:mm:ss",
    isoDateTime: "yyyy-MM-dd'T'HH:mm:ss",
    isoUtcDateTime: "UTC:yyyy-MM-dd'T'HH:mm:ss'Z'"
};

dateFormat.prototype.masks = dateFormat.masks;

// Internationalization strings
dateFormat.i18n = {
    dayNames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
};
dateFormat.prototype.i18n = dateFormat.i18n;

// For convenience...
Date.prototype.format = function format(mask, utc) {
    return dateFormat(this, mask, utc);
};

(function (Selenium, CommandBuilders) {
    Command && Command.loadAPI && Command.loadAPI();

    // DevXComboSelect
    Selenium.prototype.doDevXComboSelect = function doDevXComboSelect(locator, value) {
        /// <summary>
        /// Selecciona el primer item del combo.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="value"></param>        
        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        return this.doDevXComboSelectItem(locator, 0);
    };

    if(window['CommandDefinition']){
	// Regista la informacion del comando.
	def = new CommandDefinition('devXComboSelect');
	def.params.push({
		name: 'locator',
		description: 'Identificador del combo.'
	});
	def.comment = 'Hace click en el primer item del combo.';
	def.isAccessor = false;
	Command.functions['devXComboSelect'] = def;
    }

    if (CommandBuilders) {
        // Regista el menu contextual para el comando.
        CommandBuilders.add('action', function (window) {
            var result = {
                command: 'devXComboSelect'
            },
            recorder = this.getRecorder(window),
                element = recorder.clickedElement;

            if (element) {
                result.target = recorder.clickedElementLocators;
            } else {
                result.disabled = true;
            }
            return result;
        });
    }

    // doDevXComboSelectItem
    Selenium.prototype.doDevXComboSelectItem = function doDevXComboSelectItem(locator, value) {
        /// <summary>
        /// Selecciona el item X del combo.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="value"></param>

        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        value = value || 0;
        var me = this,
            item = String(value),
            ctl = locator.substring(0, locator.length - 2),
            ctlName = ctl.substring(3),
            imgLocator = ctl + '_B-1Img',
            itemLocator = ctl + '_DDD_L_LBI' + item + 'T0',
            timeout = 1000 * 60,
            fnCommand = function fnCommand() {
                DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments) + ' step: ' + String(arguments.callee.step));
                var ret = false;
                switch (arguments.callee.step) {
                    case 0:
                        // Valida que este visible el elemento.
                        if (me.isVisible(locator)) {
                            // Hace foco en el elemento.
                            me.doFocus(locator);
                            // Cambia de paso.
                            arguments.callee.step++;
                        }
                        break;

                    case 1:
                        // Valida que la imagen del combo este visible.
                        if (me.isVisible(imgLocator)) {
                            // Ejecuta el mouseDown sobre la imagen para que se despliegue el combo.
                            me.doMouseDown(imgLocator);
                            // Cambia de paso.
                            arguments.callee.step++;
                        }
                        break;

                    case 2:
                        // Valida que este visible el item a seleccionar.                        
                        if (me.isVisible(itemLocator)) {
                            // Selecciona el item de la lista del combo.
                            me.doMouseUp(itemLocator);
                            // Cambia de paso.
                            arguments.callee.step++;
                        }
                        break;

                    case 3:
                        // Obtiene la referencia de ventana donde se esta ejecutando la pagina. 
                        // No resolvia correctamente los IFRAMES.
                        // var win = me.browserbot.getCurrentWindow(),
                        var win = me.browserbot.findElement(itemLocator).ownerDocument.defaultView,
                            // Obtiene el Id del nodo que representa el item del combo.
                            id = itemLocator.substring(3),
                            // Obtiene el valor del item del combo.
                            itemValue = win.document.getElementById(id).textContent,
                            // Obtiene la referencia (objeto javascript) del item del combo.
                            listEditItem = win[ctlName].GetListBoxControl().GetItem(value);

                        // Setea el texto del combo. Sin esto no funciona.                             
                        win[ctlName].SetText(itemValue);
                        // Setea el item seleccionado con el objeto.
                        win[ctlName].SetSelectedItem(listEditItem);
                        // Dispara el evento de cambio de valor 
                        // para que se ejecuten los scripts asociados.
                        win[ctlName].OnChange();
                        // Cambia de paso.
                        arguments.callee.step++;
                        break;

                    case 4:
                        // Obtiene la referencia de ventana donde se esta ejecutando la pagina. 
                        // No resolvia correctamente los IFRAMES.
                        // var win = me.browserbot.getCurrentWindow(),
                        var win = me.browserbot.findElement(itemLocator).ownerDocument.defaultView;

                        // Oculta la lista desplegable del combo.
                        win[ctlName].HideDropDown();
                        // Cambia de paso.
                        arguments.callee.step++;
                        break;

                    case 5:
                        // Hace foco en el elemento.
                        me.doFocus(locator);
                        // Cambia de paso.
                        arguments.callee.step++;
                        break;

                    default:
                        ret = true;
                        break;
                }
                return ret;
            },
            fnRet = Selenium.decorateFunctionWithTimeout(fnCommand, timeout);

        fnCommand.step = 0;
        return fnRet;
    }
    
    if(window['CommandDefinition']){
	    // Regista la informacion del comando.
	    def = new CommandDefinition('devXComboSelectItem');
	    def.params.push({
		name: 'locator',
		description: 'Identificador del combo.'
	    });
	    def.params.push({
		name: 'value',
		description: 'Indice del elemento.\nEmpieza en 0.'
	    });
	    def.comment = 'Hace click en el item X del combo.\nRequiere que el control tenga asignado un clientId.';
	    def.isAccessor = false;
	    Command.functions['devXComboSelectItem'] = def;
    }

    if (CommandBuilders) {
        // Regista el menu contextual para el comando.
        CommandBuilders.add('action', function (window) {
            var result = {
                command: 'devXComboSelectItem'
            },
            recorder = this.getRecorder(window),
                element = recorder.clickedElement;

            if (element) {
                result.target = recorder.clickedElementLocators;
            } else {
                result.disabled = true;
            }
            return result;
        });
    }
    // doDevXDateSetValue
    Selenium.prototype.doDevXDateSetValue = function doDevXDateSetValue(locator, value) {
        /// <summary>
        /// Setea el Selecciona el item X del combo.
        /// </summary>
        /// <param name="locator"></param>
        /// <param name="value"></param>

        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        var me = this,
            ctl = locator.substring(0, locator.length - 2),
            ctlName = ctl.substring(3),
            timeout = 1000 * 60,
            step = {
                action: function action() {
                    DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
                    // Obtiene la referencia de ventana donde se esta ejecutando la pagina. 
                    // No resolvia correctamente los IFRAMES.
                    // var win = me.browserbot.getCurrentWindow(),                                    
                    var win = me.browserbot.findElement(locator).ownerDocument.defaultView;
                    // Setea el texto del combo. Sin esto no funciona.                             
                    win[ctlName].SetText(value);
                },
                check: function check() {
                    DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
                    return me.isVisible(locator);
                }
            };
        return this.createMultiStepCommandWithTimeout(timeout, step);
    }

    if(window['CommandDefinition']){
	    // Regista la informacion del comando.
	    def = new CommandDefinition('devXDateSetValue');
	    def.params.push({
		name: 'locator',
		description: 'Identificador del combo.'
	    });
	    def.params.push({
		name: 'value',
		description: 'Indice del elemento.\nEmpieza en 0.'
	    });
	    def.comment = 'Hace click en el item X del combo.\nRequiere que el control tenga asignado un clientId.';
	    def.isAccessor = false;
	    Command.functions['devXDateSetValue'] = def;    
	}

    if (CommandBuilders) {
        // Regista el menu contextual para el comando.
        CommandBuilders.add('action', function (window) {
            var result = {
                command: 'devXDateSetValue'
            },
            recorder = this.getRecorder(window),
                element = recorder.clickedElement;

            if (element) {
                result.target = recorder.clickedElementLocators;
            } else {
                result.disabled = true;
            }
            return result;
        });
    }

    // createMultiStepCommand
    Selenium.prototype.createMultiStepCommand = function createMultiStepCommand() {
        /// <summary>
        /// Devuelve una funcion de multiples pasos.
        /// </summary>
        /// <returns type="function">Funcion que va ejecutarse al invocarse el comando 
        /// y va ir avanzando los pasos hasta completarlos o generarse un error.</returns>

        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        var me = this,
            stepsObj = [],
            fnOk = function fnOk() {
                DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
                return true;
            },
            fnError = function fnError() {
                DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
                throw new Error('Paso invalido');
            },
            len = arguments.length;

        for (var i = 0; i < len; i++) {
            var arg = arguments[i];

            if (isObject(arg)) {
                // Verifica si el argumento es un objeto.
                stepObj = {
                    actionStep: isFunction(arg.action) ? arg.action : fnError,
                    checkConditions: isFunction(arg.check) ? arg.check : fnOk
                };
            } else if (isFunction(arg)) {
                // Verifica si el argumento es una funcion.
                stepObj = {
                    actionStep: arg,
                    checkConditions: fnOk
                };
            } else {
                // Si no es un objeto, ni es una funcion crea un paso que sale por excepcion.
                stepObj = {
                    actionStep: fnError,
                    checkConditions: fnError
                };
            }
            stepsObj.push(stepObj);
        }

        // fnMultiSteps
        var fnMultiSteps = function fnMultiSteps() {
            /// <summary>
            /// Wrapper que ejecuta los distintos pasos.
            /// </summary>
            /// <returns type="">Falso durante la ejecucion de los pasos verdadero luego 
            /// de ejecutar el ultimo paso.</returns>
            var ret = false;
            DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments) + ' Step:' + arguments.callee.step + ' Steps: ' + arguments.callee.steps.length);
            // Verifica si se ejecutaron todos los pasos.
            if (arguments.callee.step >= arguments.callee.steps.length) {
                ret = true;
            } else {
                // Obtiene la referencia del paso a ejecutar y verifica si se cumple la 
                // pre-condicion de ejecucion.
                var step = arguments.callee.steps[arguments.callee.step];
                if (step.checkConditions()) {
                    // Ejecuta el paso.
                    step.actionStep();
                    arguments.callee.step++;
                }
            }
            return ret;
        };
        fnMultiSteps.step = 0;
        fnMultiSteps.steps = stepsObj;
        return fnMultiSteps;
    }

    // createMultiStepCommandWithTimeout
    Selenium.prototype.createMultiStepCommandWithTimeout = function createMultiStepCommandWithTimeout() {
        /// <summary>
        /// Devuelve una funcion que va a ejecutar multiples pasos con timeout.
        /// </summary>
        /// <returns type="function"></returns>

        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        var args = Array.prototype.slice.call(arguments),
            timeout = args.shift(),
            fnCommand = this.createMultiStepCommand.apply(this, args);
        return Selenium.decorateFunctionWithTimeout(fnCommand, timeout);
    }

    // doStoreToday
    Selenium.prototype.doStoreToday = function doStoreToday(value, varName) {
        /// <summary>
        /// Almacena la fecha del dia con el formato dado.  
        /// </summary>
        /// <param name="value"></param>
        /// <param name="varName"></param>   
        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        if (!value || !value.length) value = 'dd/mm/yyyy';
        var d = new Date(),
            val = d.format(value);
        this.doStore(val, varName);
    }

	if(window['CommandDefinition']){
	    // Regista la informacion del comando.
	    def = new CommandDefinition('storeToday');
	    def.params.push({
		name: 'format',
		description: 'Formato.'
	    });
	    def.params.push({
		name: 'varName',
		description: 'Nombre de la variable.'
	    });
	    def.comment = 'Almacena la variable.';
	    def.isAccessor = false;
	    Command.functions['storeToday'] = def;
	}

    // doStoreTomorrow
    Selenium.prototype.doStoreTomorrow = function doStoreTomorrow(value, varName) {
        /// <summary>
        /// Almacena la fecha del dia con el formato dado.  
        /// </summary>
        /// <param name="value"></param>
        /// <param name="varName"></param>
        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        if (!value || !value.length) value = 'dd/mm/yyyy';
        var dt = new Date(),
            d = dateAdd(dt, 'D', 1),
            val = d.format(value);
        DEBUG(arguments.callee.name + ' vars: ' + JSON.stringify({
            value: value,
            varName: varName,
            dt: dt,
            d: d,
            val: val
        }));
        this.doStore(val, varName);
    }

    if(window['CommandDefinition']){
	    // Regista la informacion del comando.
	    def = new CommandDefinition('storeTomorrow');
	    def.params.push({
		name: 'format',
		description: 'Formato.'
	    });
	    def.params.push({
		name: 'varName',
		description: 'Nombre de la variable.'
	    });
	    def.comment = 'Almacena la variable.';
	    def.isAccessor = false;
	    Command.functions['storeTomorrow'] = def;
    }

    // doStoreNextWeek
    Selenium.prototype.doStoreNextWeek = function doStoreNextWeek(value, varName) {
        /// <summary>
        /// Almacena la fecha de la semana que viene con el formato dado.  
        /// </summary>
        /// <param name="value"></param>
        /// <param name="varName"></param>
        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        if (!value || !value.length) value = 'dd/mm/yyyy';
        var dt = new Date(),
            d = dateAdd(dt, 'D', 7),
            val = d.format(value);
        DEBUG(arguments.callee.name + ' vars: ' + JSON.stringify({
            value: value,
            varName: varName,
            dt: dt,
            d: d,
            val: val
        }));
        this.doStore(val, varName);
    }

    if(window['CommandDefinition']){
	    // Regista la informacion del comando.
	    def = new CommandDefinition('storeNextWeek');
	    def.params.push({
		name: 'format',
		description: 'Formato.'
	    });
	    def.params.push({
		name: 'varName',
		description: 'Nombre de la variable.'
	    });
	    def.comment = 'Almacena la variable.';
	    def.isAccessor = false;
	    Command.functions['storeNextWeek'] = def;
    }

    // isObject
    function isObject(v) {
        /// <summary>
        /// Evalua si el parametro recibido es un objeto.
        /// </summary>
        /// <param name="v">Valor a evaluar si es un objeto.</param>
        /// <returns type="">Verdadero si el parametro es un objeto.</returns>
        return !!v && Object.prototype.toString.call(v) === '[object Object]';
    }

    // isFunction
    function isFunction(v) {
        /// <summary>
        /// Evalua si el parametro recibido es una funcion.
        /// </summary>
        /// <param name="v">Valor a evaluar si es una funcion.</param>
        /// <returns type="">Verdadero si el parametro es un funcion.</returns>
        return Object.prototype.toString.apply(v) === '[object Function]';
    }

    function DEBUG(value) {
        Selenium.prototype.doEcho.call(null, value);
    }

    function dateAdd(objDate, strInterval, intIncrement) {
        DEBUG(arguments.callee.name + ' arguments: ' + JSON.stringify(arguments));
        if (typeof (objDate) == "string") {
            objDate = new Date(objDate);

            if (isNaN(objDate)) {
                throw ("DateAdd: Date is not a valid date");
            }
        } else if (typeof (objDate) != "object" || objDate.constructor.toString().indexOf("Date()") == -1) {
            throw ("DateAdd: First parameter must be a date object");
        }

        if (strInterval != "M" && strInterval != "D" && strInterval != "Y" && strInterval != "h" && strInterval != "m" && strInterval != "uM" && strInterval != "uD" && strInterval != "uY" && strInterval != "uh" && strInterval != "um" && strInterval != "us") {
            throw ("DateAdd: Second parameter must be M, D, Y, h, m, uM, uD, uY, uh, um or us");
        }

        if (typeof (intIncrement) != "number") {
            throw ("DateAdd: Third parameter must be a number");
        }

        switch (strInterval) {
            case "M":
                objDate.setMonth(parseInt(objDate.getMonth()) + parseInt(intIncrement));
                break;

            case "D":
                objDate.setDate(parseInt(objDate.getDate()) + parseInt(intIncrement));
                break;

            case "Y":
                objDate.setYear(parseInt(objDate.getYear()) + parseInt(intIncrement));
                break;

            case "h":
                objDate.setHours(parseInt(objDate.getHours()) + parseInt(intIncrement));
                break;

            case "m":
                objDate.setMinutes(parseInt(objDate.getMinutes()) + parseInt(intIncrement));
                break;

            case "s":
                objDate.setSeconds(parseInt(objDate.getSeconds()) + parseInt(intIncrement));
                break;

            case "uM":
                objDate.setUTCMonth(parseInt(objDate.getUTCMonth()) + parseInt(intIncrement));
                break;

            case "uD":
                objDate.setUTCDate(parseInt(objDate.getUTCDate()) + parseInt(intIncrement));
                break;

            case "uY":
                objDate.setUTCFullYear(parseInt(objDate.getUTCFullYear()) + parseInt(intIncrement));
                break;

            case "uh":
                objDate.setUTCHours(parseInt(objDate.getUTCHours()) + parseInt(intIncrement));
                break;

            case "um":
                objDate.setUTCMinutes(parseInt(objDate.getUTCMinutes()) + parseInt(intIncrement));
                break;

            case "us":
                objDate.setUTCSeconds(parseInt(objDate.getUTCSeconds()) + parseInt(intIncrement));
                break;
        }
        return objDate;
    }

})(Selenium, ((window['CommandBuilders']) ? CommandBuilders : null), Command);