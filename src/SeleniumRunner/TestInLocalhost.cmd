@ECHO OFF
SETLOCAL

SET config=LOCALHOST
SET domain=localhost

CALL bigtext.cmd Test %config%

CALL :EchoC Blue "Desea continuar con los test en el servidor %config%?"
ECHO %domain%

CHOICE /T 10 /C yc /M "Presione Y para continuar, C para Cancelar." /D c
IF %ERRORLEVEL% EQU 2 (
	ECHO.
	CALL :EchoC Red "Actualizacion cancelada"
	ECHO.
	GOTO salir
)
CALL TestInServer.cmd %config% %domain% 4445
GOTO salir

:EchoC
REM %1 Color
REM %2 Mensaje
SET color=%1
SET msg=%2
SET msg=%msg:"=%
%Windir%\System32\WindowsPowerShell\v1.0\Powershell.exe write-host -foregroundcolor %color% %msg%
GOTO :eof

:Salir
ENDLOCAL
PAUSE