@ECHO OFF
SETLOCAL

SET config=%1
SET domain=%2
SET port=%3

IF [%config%] EQU [] GOTO Salir
IF [%domain%] EQU [] GOTO Salir

SET PackagePath=%CD%\Deploy\Packages
IF NOT EXIST %PackagePath%\version ECHO 190001010100>%PackagePath%\version
SET /P Version=<%PackagePath%\version
IF [%Version%] EQU [] SET Version=190001010100
FOR /F "tokens=1-4 delims=/ " %%a IN ('DATE /T') DO (SET cDate=%%c%%b%%a)
FOR /F "tokens=1-2 delims=/:" %%a IN ("%TIME%") DO (SET cTime=%%a%%b).

REM SET lastVersion=%cDate%%cTime%
SET fecha=%cDate: =0%%cTime: =0%
SET AllSupportBrowsers=googlechrome firefox mock firefoxproxy pifirefox chrome iexploreproxy iexplore firefox3 safariproxy googlechrome konqueror firefox2 safari piiexplore firefoxchrome opera webdriver iehta custom
SET SupportBrowsers=firefox iexplore googlechrome iehta

SET screenshot=%CD%\Log\To%config%_%Version%_%fecha%.png

REM Ejecuta las pruebas
CALL :EchoC Green "Ejecuta el los test"
CALL :LaunchTests %Version% %domain%

ENDLOCAL

GOTO salir

:Browse
REM %1 Product
REM %2 domain
REM %3 File

NIRCMDC SHEXEC "open" "http://%1.%2/%3"
GOTO :eof

:Test
REM %1 Browser
REM %2 Product
REM %3 Suite
REM %4 Version
REM %5 domain

SET title=%config% Test in %1 (%port%) - %2.%3 [%5] %4
SET tf=%CD%\Test

CALL :EchoC Green "%title%"
START "%title%" /MIN /HIGH LaunchTestInServer.cmd "%1" "%2" "%3" "%4" "%5" "%config%" "%port%"

SET /A port+=1
GOTO :eof

:LaunchTests
REM %1 %Version% 
REM %2 %domain%

REM FOR %%a IN (%SupportBrowsers%) DO CALL :Test %%a app TestSuite %Version% %domain%
FOR %%a IN (%SupportBrowsers%) DO CALL :Test %%a app TestSuite %1 %2
GOTO :eof

:EchoC
REM %1 Color
REM %2 Mensaje

SET color=%1
SET msg=%2
SET msg=%msg:"=%
%Windir%\System32\WindowsPowerShell\v1.0\Powershell.exe write-host -foregroundcolor %color% %msg%

GOTO :eof

:CleanOldLog
FOR %%a IN (%AllSupportBrowsers%) DO CALL :DeleteOldLog %CD%\Test\%config%.Selenium.*.%%a.log.lck
FOR %%a IN (%AllSupportBrowsers%) DO CALL :DeleteOldLog %CD%\Test\%config%.Selenium.*.%%a.log
FOR %%a IN (%AllSupportBrowsers%) DO CALL :DeleteOldLog %CD%\Test\%config%.app.TestSuite.TestResults.*.%%a

GOTO :eof

:DeleteOldLog
SET cnt=0
DIR /D /S /B "%1">ToSort
SORT /R ToSort>Sorted
FOR /F "tokens=*" %%a IN (Sorted) DO CALL :DeleteAction %%a
GOTO :EOF

GOTO salir

:DeleteAction
SET /A cnt+=1
IF %cnt% GTR 10 ( 
	REM ECHO %cnt% %1
	DEL /F %1
)
GOTO :EOF

:Error
COLOR C
ECHO Forma de uso
ECHO.
ECHO TestServer.cmd [Configuracion] [Server] [Port]
ECHO 	[Configuracion]: Nombre de la configuracion.
ECHO 	[Server]: Nombre o IP del servidor en Formato \\Servidor.
ECHO 	[Port]: Puerto Base
ECHO.
GOTO salir

:Salir
CALL :CleanOldLog
REM PAUSE