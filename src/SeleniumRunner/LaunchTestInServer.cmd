@ECHO OFF
REM %1 Browser
REM %2 Product
REM %3 Suite
REM %4 Version
REM %5 domain
REM %6 config
REM %7 port


SET browser=%1
SET browser=%browser:"=%
SET product=%2
SET product=%product:"=%
SET suite=%3
SET suite=%suite:"=%
SET version=%4
SET version=%version:"=%
SET domain=%5
SET domain=%domain:"=%
SET config=%6
SET config=%config:"=%
SET port=%7
SET port=%port:"=%

SET tf=%CD%\Test
SET results=%tf%\%config%.%product%.%suite%.TestResults.%version%.%browser%.html
java -jar %CD%\selenium-server-standalone-2.25.0.jar -htmlSuite "*%browser%" "http://%product%.%domain%" "%tf%\%suite%.html" "%results%" -port %port% -multiwindow -log %tf%\%config%.Selenium.%version%.%browser%.log -browserSideLog -userExtensions %CD%\user-extensions.js
NIRCMDC SHEXEC "open" "file:///%results%"
EXIT