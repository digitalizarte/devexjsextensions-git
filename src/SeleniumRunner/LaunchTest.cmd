@ECHO OFF
REM PROMPT $
SETLOCAL
FOR /F "tokens=1-4 delims=/ " %%a IN ('DATE /T') DO (SET cDate=%%c%%b%%a)
FOR /F "tokens=1-2 delims=/:" %%a IN ("%TIME%") DO (SET cTime=%%a%%b)
SET config=CAPCOM
SET Version=%cDate%%cTime%
SET domain=dottransfers.sommytech.com.ar
SET port=7445
CALL :Test firefox app WarmUpSuite %Version% %domain%
ENDLOCAL
GOTO Salir

:Test
REM %1 Browser
REM %2 Product
REM %3 Suite
REM %4 Version
REM %5 domain

SET title=%config% Test in %1 (%port%) - %2.%3 (%4)
SET tf=%CD%\Test
START "%title%" /MIN /HIGH java -jar %CD%\selenium-server-standalone-2.25.0.jar -htmlSuite "*%1" "http://%2.%5" "%tf%\%3.html" "%tf%\%config%.%2.%3.TestResults.%4.%1.html" -port %port% -multiwindow -log %tf%\%config%.Selenium.%4.%1.log -browserSideLog -userExtensions %CD%\user-extensions.js
SET /A port+=1
GOTO :eof

:Salir
REM Pause